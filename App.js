import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import * as ScreenOrientation from 'expo-screen-orientation';
import Routes from './src/routes/index';
import { SafeAreaProvider } from 'react-native-safe-area-context';

class App extends React.Component {
  componentDidMount(){
    this.changeOrientation();
  }

  async changeOrientation(){
    await ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.LANDSCAPE);
  }

  render(){
    return (
      <React.Fragment>
          <StatusBar hidden={true} />
          <Routes/>
      </React.Fragment>
    );
  }
}

export default App;
