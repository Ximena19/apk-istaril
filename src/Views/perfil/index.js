import React from 'react';
import { Image, Text, Animated } from 'react-native'; 
import Navigation from '../../shared/navigation';
import { fadeInBottom, ImageFadeInBottom } from '../../animations/FadeInBottom';
import { fadeInLeft, ImageFadeInLeft } from '../../animations/FadeInLeft';


//Components
import Style from './styles/style';

class Perfil extends React.Component {
    constructor(props){ 
        super(props);
        this.state = { 
            opacity: new Animated.Value(0),
            opacity1: new Animated.Value(0),
            transform: new Animated.Value(0),
            opacity2: new Animated.Value(0),
            transform1: new Animated.Value(0),
            opacity3: new Animated.Value(0),
            transform2: new Animated.Value(0)
        }
        this.Animations = this.Animations.bind(this)
        
    }

    componentDidMount(){
        this.Animations()
    }

    Animations(){
        fadeInLeft({opacity: this.state.opacity, transform: this.state.transform});
        fadeInBottom({opacity: this.state.opacity3, transform: this.state.transform2});
    }

    render(){
        return(
            <React.Fragment>
                <Image  source={require('../../assets/portada110.jpg')} style={Style.image}/> 
                <ImageFadeInLeft source={require('../../assets/por1.png')} styles={ Style.por1 } state={{ opacity: this.state.opacity, transform: this.state.transform }}/> 
                <ImageFadeInBottom source={require('../../assets/portada10.png')} styles={ Style.por2 } state={{ opacity: this.state.opacity3, transform: this.state.transform2 }}/> 
                <Image source={require('../../assets/menu.png')} style={Style.por3} /> 

                <Navigation routes={{ box1: 'Cerebro2', box2: 'Indice', box3: 'Perfil2' }} props={ this.props } />   
            </React.Fragment>
        );   
    }
}

export default Perfil;