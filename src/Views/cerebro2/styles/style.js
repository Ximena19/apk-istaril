import { StyleSheet } from "react-native";

const Style = StyleSheet.create({
    image: {
        resizeMode: 'stretch',
        width: '100%',
        height: '100%',
        backgroundColor: 'black'
    },

    pol3: {
        resizeMode: 'stretch',
        width: '51%',
        height: '10%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '3%',
        left: '10%'
    },
    pol1: {
        resizeMode: 'stretch',
        width: '65%',
        height: '40%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '30%',
        left: '8%'
    },
    pol4: {
        resizeMode: 'stretch',
        width: '12%',
        height: '50%',
        position: 'absolute',
        zIndex: 1,
        top: '25%',
        right: '13%'
    },
    pol5: {
        width: '12%',
        height: '15%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '2%',
        right: '10%'
    },
});

export default Style;
