import React from 'react'
import { Animated, Image, View } from 'react-native';
import Navigation from '../../shared/navigation';
import Style from './styles/style';
import { fadeInLeft, ImageFadeInLeft } from '../../animations/FadeInLeft';


//Components
class Indice extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            opacity: new Animated.Value(0),
            transform: new Animated.Value(0),
        }
        this.Animations = this.Animations.bind(this)
    }
    componentDidMount(){
        this.Animations()
    }

    Animations(){
        fadeInLeft({opacity: this.state.opacity, transform: this.state.transform});

    }

    render() {
        return (
            <React.Fragment>
                <Image source={require('../../assets/portada2.jpg')} style={Style.image} />
                <ImageFadeInLeft  source={require('../../assets/indice1.png')} styles={Style.image1} state={{ opacity: this.state.opacity, transform: this.state.transform }}/> 
                <ImageFadeInLeft  source={require('../../assets/indice2.png')} styles={Style.image3} state={{ opacity: this.state.opacity, transform: this.state.transform }}/> 
                <Image source={require('../../assets/menu.png')} style={Style.image2} />
                <View style={Style.cuadro1} onStartShouldSetResponder={() => this.props.navigation.navigate('Definicion')}></View>
                <View style={Style.cuadro2} onStartShouldSetResponder={() => this.props.navigation.navigate('Sobrepeso')}></View>
                <View style={Style.cuadro3} onStartShouldSetResponder={() => this.props.navigation.navigate('Riesgo')}></View>
                <View style={Style.cuadro4} onStartShouldSetResponder={() => this.props.navigation.navigate('Evolucion')}></View>
                <View style={Style.cuadro5} onStartShouldSetResponder={() => this.props.navigation.navigate('Mujeres')}></View>
                <View style={Style.cuadro6} onStartShouldSetResponder={() => this.props.navigation.navigate('Cerebro')}></View>
                <View style={Style.cuadro7} onStartShouldSetResponder={() => this.props.navigation.navigate('Perfil')}></View> 
                <View style={Style.cuadro8} onStartShouldSetResponder={() => this.props.navigation.navigate('Istaril')}></View>
                <View style={Style.cuadro9} onStartShouldSetResponder={() => this.props.navigation.navigate('Semanas')}></View> 
                <View style={Style.cuadro10} onStartShouldSetResponder={() => this.props.navigation.navigate('Beneficios')}></View>
                <View style={Style.cuadro11} onStartShouldSetResponder={() => this.props.navigation.navigate('Tasa')}></View>
                <View style={Style.cuadro12} onStartShouldSetResponder={() => this.props.navigation.navigate('Reduccion')}></View>  
                <Navigation routes={{ box1: 'Home', box2: 'Home', box3: 'Definicion' }} props={ this.props } />   
            </React.Fragment> 
        )
    }
}

export default Indice;