import { StyleSheet } from "react-native";

const Style = StyleSheet.create({
    image: {
        resizeMode: 'stretch',
        width: '100%',
        height: '100%',
        backgroundColor: 'black'
    },

    por1: {
        resizeMode: 'stretch',
        width: '65%',
        height: '10%',
        position: 'absolute',
        zIndex: 1,
        top: '4%',
        left: '8%'
    },
    por2: {
        resizeMode: 'stretch',
        width: '78%',
        height: '30%',
        position: 'absolute',
        zIndex: 1,
        top: '18%',
        left: '8%'
    },
    
    por4: {
        resizeMode: 'stretch',
        width: '32%',
        height: '40%',
        position: 'absolute',
        zIndex: 1,
        top: '48%',
        left: '7%'
    },
    por5: {
        resizeMode: 'stretch',
        width: '32%',
        height: '40%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '51%',
        left: '33%'
    },
    por6: {
        resizeMode: 'stretch',
        width: '32%',
        height: '40%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '54%',
        right: '9%'
    },
    por3: {
        resizeMode: 'stretch',
        width: '15%',
        height: '25%',
        position: 'absolute',
        zIndex: 1,
        top: '30%',
        right: '10%'
    },
    por7: {
        width: '12%',
        height: '15%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '2%',
        right: '10%'
    },
});

export default Style;
