import { StyleSheet } from "react-native";

const Style = StyleSheet.create({
    image: {
        resizeMode: 'stretch',
        width: '100%',
        height: '100%',
        backgroundColor: 'black'
    },

    por1: {
        resizeMode: 'stretch',
        width: '60%',
        height: '10%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '3%',
        left: '10%'
    },
    por2: {
        resizeMode: 'stretch',
        width: '80%',
        height: '10%',
        backgroundColor: '#00000033',
        position: 'absolute',
        zIndex: 1,
        top: '22%',
        left: '0%'
    },
    
    por4: {
        resizeMode: 'stretch',
        width: '30%',
        height: '8%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '18%',
        left: '38%'
    },
    por3: {
        resizeMode: 'stretch',
        width: '65%',
        height: '50%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '30%',
        left: '5%'
    },
    por5: {
        width: '12%',
        height: '15%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '2%',
        right: '10%'
    },
    image1: {
        resizeMode: 'stretch',
        width: '19%',
        height: '45%',
        position: 'absolute',
        zIndex: 1,
        top: '34%',
        left: '71%'
    },
});

export default Style;
