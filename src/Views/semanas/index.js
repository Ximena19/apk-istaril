import React from 'react';
import { Image, Text, Animated } from 'react-native'; 
import Navigation from '../../shared/navigation';

import { fadeInLeft, ImageFadeInLeft } from '../../animations/FadeInLeft';
import { fadeInRight, ImageFadeInRight } from '../../animations/FadeInRight';
import { fadeIn, ImageFadeIn } from '../../animations/FadeIn';
import { LeftView, Left } from '../../animations/Left/index';


//Components
import Style from './styles/style';

class Semanas extends React.Component {
    constructor(props){ 
        super(props);
        this.state = { 
            opacity: new Animated.Value(0),
            opacity1: new Animated.Value(0),
            transform: new Animated.Value(0),
            opacity2: new Animated.Value(0),
            transform1: new Animated.Value(0),
            opacity3: new Animated.Value(0),
            transform2: new Animated.Value(0),
            lineOpacity: new Animated.Value(0),
            lineTransform: new Animated.Value(0)
        }
        this.Animations = this.Animations.bind(this)  
        
    }

    componentDidMount(){
        this.Animations()
    }

    Animations(){  
        const { lineOpacity, lineTransform } = this.state;
        fadeInLeft({opacity: this.state.opacity, transform: this.state.transform});
        fadeIn(this.state.opacity1);
        fadeInRight({opacity: this.state.opacity2, transform: this.state.transform1}); 
        Left({ opacity: lineOpacity, transform: lineTransform });
    }

    render(){
        return(
            <React.Fragment>
                <Image  source={require('../../assets/pesso.jpg')} style={Style.image}/> 
                <ImageFadeInLeft source={require('../../assets/peso1.png')} styles={ Style.por1 } state={{ opacity: this.state.opacity, transform: this.state.transform }}/> 
                <ImageFadeIn source={require('../../assets/peso2.png')} styles={ Style.por4 } state={this.state.opacity1}/> 
                <LeftView source={require('../../assets/peso.png')} styles={ Style.por3 } state={{ opacity: this.state.lineOpacity, transform: this.state.lineTransform }}/>
                <ImageFadeIn  source={require('../../assets/perdida.png')} styles={Style.image1}  state={this.state.opacity1}/> 

                <Image source={require('../../assets/menu.png')} style={Style.por5} />  

                <Navigation routes={{ box1: 'Istaril2', box2: 'Indice', box3: 'Semanas2' }} props={ this.props } />   
            </React.Fragment>
        );   
    }
}

export default Semanas ;