import { StyleSheet } from "react-native";

const Style = StyleSheet.create({
    image: {
        resizeMode: 'stretch',
        width: '100%',
        height: '100%',
    },

    por1: {
        resizeMode: 'stretch',
        width: '60%',
        height: '10%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '3%',
        left: '8%'
    },
    por2: {
        resizeMode: 'stretch',
        width: '75%',
        height: '50%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '30%',
        left: '9%'
    },
    
    por4: {
        resizeMode: 'contain',
        width: '20%',
        height: '20%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '9%',
        left: '36%'
    },
    por5: {
        width: '12%',
        height: '15%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '2%',
        right: '10%'
    },
    image1: {
        resizeMode: 'stretch',
        width: '11%',
        height: '31%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '17.2%',
        left: '14%'
    },
    image2: {
        resizeMode: 'stretch',
        width: '8%',
        height: '43%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '16.3%',
        left: '24%'
    },
    image3: {
        resizeMode: 'stretch',
        width: '17%',
        height: '40%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '17.5%',
        left: '31%'
    },
    image4: {
        resizeMode: 'stretch',
        width: '12.5%',
        height: '30%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '18.5%',
        left: '48%'
    },
    image5: {
        resizeMode: 'stretch',
        width: '13.5%',
        height: '40%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '16%',
        right: '24.5%'
    },
    image6: {
        resizeMode: 'stretch',
        width: '10%',
        height: '53%',
        backgroundColor: 'transparent',
        position: 'absolute',  
        zIndex: 1,
        top: '19.2%',
        right: '14.5%'
    },
});

export default Style;
