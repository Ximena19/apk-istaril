import { StyleSheet } from "react-native";

const Style = StyleSheet.create({
    image: {
        resizeMode: 'stretch',
        width: '100%',
        height: '100%',
        backgroundColor: 'black'
    },

    pol3: {
        resizeMode: 'stretch',
        width: '55%',
        height: '13%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '2%',
        left: '10%'
    },
    pol1: {
        resizeMode: 'stretch',
        width: '83%',
        height: '65%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '19%',
        left: '10%'
    },
    pol2: {
        resizeMode: 'stretch',
        width: '35%',
        height: '5%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        bottom: '16%',
        left: '9%'
    },
    pol4: {
        width: '12%',
        height: '15%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '2%',
        right: '10%'
    },
});

export default Style;
