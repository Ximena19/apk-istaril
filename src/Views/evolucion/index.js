import React from 'react';
import { Image, Text, Animated } from 'react-native'; 
import Navigation from '../../shared/navigation';
import { fadeInLeft, ImageFadeInLeft } from '../../animations/FadeInLeft';
import { fadeInRight, ImageFadeInRight } from '../../animations/FadeInRight';
import { fadeIn, ImageFadeIn } from '../../animations/FadeIn';
import { fadeInBottom, ImageFadeInBottom } from '../../animations/FadeInBottom';
import { UpView, Up } from '../../animations/Up/index';
import { Pulse, ImagePulse } from '../../animations/Pulse';





//Components
import Style from './styles/style';

class Evolucion extends React.Component {
    constructor(props){
        super(props);
        this.state = { 
            /** Fade In Left States */
            opacityFadeInLeft: new Animated.Value(0),
            transformFadeInLeft: new Animated.Value(0),

            /** Fade In States */
            opacity: new Animated.Value(0),
            opacity1: new Animated.Value(0),

            /** Fade In Right States */
            opacityFadeInRight: new Animated.Value(0),
            transformFadeInRight: new Animated.Value(0),

            /** Up Lines States (Line 1) */
            line1Opacity: new Animated.Value(0),
            line1Transform: new Animated.Value(-0.1),
    
            /** Up Lines States (Line 2) */
            line2Opacity: new Animated.Value(0),
            line2Transform: new Animated.Value(-0.1),
    
            /** Up Lines States (Line 3) */
            line3Opacity: new Animated.Value(0),
            line3Transform: new Animated.Value(-0.1),
    
            /** Up Lines States (Line 4) */
            line4Opacity: new Animated.Value(0),
            line4Transform: new Animated.Value(-0.1),
        }
        this.Animations = this.Animations.bind(this)
        
    }

    componentDidMount(){
        this.Animations()
    }
    Animations(){
        fadeInLeft({ opacity: this.state.opacityFadeInLeft, transform: this.state.transformFadeInLeft });
        fadeIn(this.state.opacity); 
        fadeIn(this.state.opacity1); 
        fadeInRight({ opacity: this.state.opacityFadeInRight, transform: this.state.transformFadeInRight });
        // fadeIn(this.state.opacity1);
        // Pulse(this.state.pulse);
        let bandera = 0;
        const {
            line1Opacity,
            line1Transform,
            line2Opacity,
            line2Transform,
            line3Opacity,
            line3Transform,
            line4Opacity,
            line4Transform
        } = this.state;
        let i = setInterval(function(){
            if(bandera == 0){
                Up({ opacity: line1Opacity, transform: line1Transform });
                bandera++
            }else if(bandera == 1){
                Up({ opacity: line2Opacity, transform: line2Transform });
                bandera++
            }else if(bandera == 2){
                Up({ opacity: line3Opacity, transform: line3Transform });
                bandera++
            }else if(bandera == 3){
                Up({ opacity: line4Opacity, transform: line4Transform });
                bandera++
            }else if(bandera == 4){
                clearInterval(i);
            }
        }, 2000);
    }

    render(){
        return(
            <React.Fragment>
                <Image  source={require('../../assets/portada6.jpg')} style={Style.image}/> 
                <ImageFadeInLeft source={require('../../assets/font1.png')} styles={ Style.image1 } state={{ opacity: this.state.opacityFadeInLeft, transform: this.state.transformFadeInLeft }}/>
                <ImageFadeIn source={require('../../assets/font2.png')} styles={ Style.image2 } state={this.state.opacity}/>

                {/* graficas */}
                <UpView  source={require('../../assets/recur1.png')} styles={Style.image6} state={{ opacity: this.state.line1Opacity, transform:this.state.line1Transform }}/> 
                <UpView  source={require('../../assets/recur2.png')} styles={Style.image7} state={{ opacity: this.state.line2Opacity, transform:this.state.line2Transform }}/> 
                <UpView  source={require('../../assets/recurs3.png')} styles={Style.image8} state={{ opacity: this.state.line3Opacity, transform:this.state.line3Transform }}/> 
                <UpView  source={require('../../assets/recur4.png')} styles={Style.image9} state={{ opacity: this.state.line4Opacity, transform:this.state.line4Transform }}/> 
                
                {/* boton */}
                <ImageFadeIn  source={require('../../assets/Recurs.png')} styles={Style.image10} state={this.state.opacity1}/> 
                <ImageFadeIn  source={require('../../assets/Recurs1.png')} styles={Style.image11} state={this.state.opacity1}/> 
                <ImageFadeIn  source={require('../../assets/Recurs2.png')} styles={Style.image12} state={this.state.opacity1}/> 
                <ImageFadeIn  source={require('../../assets/Recurs4.png')} styles={Style.image13} state={this.state.opacity1}/> 


                <Image source={require('../../assets/font3.png')} style={ Style.image3 } /> 
                <ImageFadeInRight source={require('../../assets/font4.png')} styles={ Style.image4 } state={{ opacity: this.state.opacityFadeInRight, transform: this.state.transformFadeInRight }}/> 
                <Image source={require('../../assets/menu.png')} style={Style.image5} />  

                <Navigation routes={{ box1: 'Riesgo', box2: 'Indice', box3: 'Mujeres' }} props={ this.props } />   
            </React.Fragment>
        );   
    }
}

export default Evolucion;