import { StyleSheet } from "react-native";

const Style = StyleSheet.create({
    image: {
        resizeMode: 'stretch',
        width: '100%',
        height: '100%'
    },

    image1: {
        resizeMode: 'stretch',
        width: '71%',
        height: '12%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '3%',
        left: '8%'
    },
    image2: {
        resizeMode: 'stretch',
        width: '15%',
        height: '24%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 2,
        top: '22%',
        left: '0%'
    },
    image3: {
        resizeMode: 'stretch',
        width: '65%',
        height: '60%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '25%',
        left: '15%'
    },
    image4: {
        resizeMode: 'stretch',
        width: '10%',
        height: '17%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        bottom: '25%',
        right: '3%'
    },
    image5: {
        width: '12%',
        height: '6%',
        resizeMode: 'stretch',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '6.2%',
        right: '10%'
    },

    image6: {
        width: '7%',
        height: '34%',    
        resizeMode: 'stretch',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 2,
        bottom: '25.3%',
        right: '70%'
    },
    image7: {
        width: '12%',
        height: '30%',
        resizeMode: 'stretch',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 2,
        bottom: '25.4%',
        right: '56.2%'
    },
    image8: {
        width: '18%',
        height: '28%',
        resizeMode: 'stretch',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 2,
        bottom: '25.5%',
        right: '35.4%'
    },
    image9: {
        width: '12%',
        height: '23%',
        resizeMode: 'stretch',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 2,
        bottom: '25.3%',
        right: '20%'
    },
    image10: {
        width: '10%',
        height: '13%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 2,
        bottom: '62%',
        right: '68%'
    },
    image11: {
        width: '16%',
        height: '15%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 2,
        bottom: '58%',
        right: '54%'
    },
    image12: {
        width: '16%',
        height: '16%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 2,
        bottom: '55%',
        right: '36%'
    },
    image13: {
        width: '14%',
        height: '14%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 2,
        bottom: '51%',
        right: '19%'
    },
});

export default Style;
