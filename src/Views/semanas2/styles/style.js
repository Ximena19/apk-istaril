import { StyleSheet } from "react-native";

const Style = StyleSheet.create({
    image: {
        resizeMode: 'stretch',
        width: '100%',
        height: '100%'
    },

    por1: {
        resizeMode: 'stretch',
        width: '65%',
        height: '9%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '3%',
        left: '10%'
    },
    por2: {
        resizeMode: 'contain',
        width: '80%',
        height: '60%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '22%',
        left: '10 %'
    },
    
    por4: {
        resizeMode: 'stretch',
        width: '50%',
        height: '8%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '18%',
        left: '20%'
    },
    por5: {
        width: '12%',
        height: '15%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '2%',
        right: '10%'
    },
});

export default Style;
