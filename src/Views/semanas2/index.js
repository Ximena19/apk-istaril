import React from 'react';
import { Image, Text, Animated } from 'react-native'; 
import Navigation from '../../shared/navigation';
import { fadeInLeft, ImageFadeInLeft } from '../../animations/FadeInLeft';
import { fadeIn, ImageFadeIn } from '../../animations/FadeIn';
import { fadeInBottom, ImageFadeInBottom } from '../../animations/FadeInBottom';


//Components
import Style from './styles/style';

class Semanas2 extends React.Component {
    constructor(props){ 
        super(props);
        this.state = { 
            opacity: new Animated.Value(0),
            opacity1: new Animated.Value(0),
            transform: new Animated.Value(0),
            opacity2: new Animated.Value(0),
            transform1: new Animated.Value(0),
            opacity3: new Animated.Value(0),
            transform2: new Animated.Value(0)
        }
        this.Animations = this.Animations.bind(this)
        
    }

    componentDidMount(){
        this.Animations()
    }

    Animations(){
        fadeInLeft({opacity: this.state.opacity, transform: this.state.transform});
        fadeIn(this.state.opacity1);
        fadeInBottom({opacity: this.state.opacity3, transform: this.state.transform2});
    }

    render(){
        return(
            <React.Fragment>
                <Image  source={require('../../assets/portada15.png')} style={Style.image}/> 
                <ImageFadeIn source={require('../../assets/recurso10.png')} styles={ Style.por1 } state={this.state.opacity1}/> 
                <ImageFadeInLeft source={require('../../assets/recurso11.png')} styles={ Style.por4 } state={{ opacity: this.state.opacity, transform: this.state.transform }}/> 
                <ImageFadeInBottom source={require('../../assets/recurso12.png')} styles={ Style.por2 } state={{ opacity: this.state.opacity3, transform: this.state.transform2 }}/> 
                <Image source={require('../../assets/menu.png')} style={Style.por5} /> 

                <Navigation routes={{ box1: 'Semanas', box2: 'Indice', box3: 'Beneficios' }} props={ this.props } />   
            </React.Fragment>
        );   
    }
}

export default Semanas2 ;