import React from 'react';
import { Image, Text, Animated } from 'react-native'; 
import Navigation from '../../shared/navigation';
import { fadeInRight, ImageFadeInRight } from '../../animations/FadeInRight';
import { fadeIn, ImageFadeIn } from '../../animations/FadeIn';


//Components
import Style from './styles/style';

class Perfil2 extends React.Component {
    constructor(props){ 
        super(props);
        this.state = { 
            opacity: new Animated.Value(0),
            opacity1: new Animated.Value(0),
            transform: new Animated.Value(0),
            opacity2: new Animated.Value(0),
            transform1: new Animated.Value(0),
            opacity3: new Animated.Value(0),
            transform2: new Animated.Value(0)
        }
        this.Animations = this.Animations.bind(this)
        
    }

    componentDidMount(){
        this.Animations()
    }

    Animations(){
        fadeIn(this.state.opacity1);
        fadeInRight({opacity: this.state.opacity2, transform: this.state.transform1});
    }

    render(){
        return(
            <React.Fragment>
                <Image  source={require('../../assets/portada110.jpg')} style={Style.image}/> 
                <ImageFadeIn source={require('../../assets/por1.png')} styles={ Style.por1 } state={this.state.opacity1}/> 
                <ImageFadeInRight source={require('../../assets/sol.png')} styles={ Style.por2 } state={{ opacity: this.state.opacity2, transform: this.state.transform1 }}/> 
                <Image source={require('../../assets/menu.png')} style={Style.por3} />

                <Navigation routes={{ box1: 'Perfil', box2: 'Indice', box3: 'Istaril' }} props={ this.props } />   
            </React.Fragment>
        );   
    }
}

export default Perfil2 ;