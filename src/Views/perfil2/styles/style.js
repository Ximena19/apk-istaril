import { StyleSheet } from "react-native";

const Style = StyleSheet.create({
    image: {
        resizeMode: 'stretch',
        width: '100%',
        height: '100%'
    },

    por1: {
        resizeMode: 'stretch',
        width: '50%',
        height: '10%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '3%',
        left: '9%'
    },
    por2: {
        resizeMode: 'stretch',
        width: '92%',
        height: '65%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '18%',
        left: '2%'
    },
    por3: {
        width: '12%',
        height: '15%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '2%',
        right: '10%'
    },
});

export default Style;
