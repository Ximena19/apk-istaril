import React from 'react';
import { Image, Text, Animated } from 'react-native'; 
import Navigation from '../../shared/navigation';
import { fadeInLeft, ImageFadeInLeft } from '../../animations/FadeInLeft';
import { fadeInRight, ImageFadeInRight } from '../../animations/FadeInRight';
import { fadeIn, ImageFadeIn } from '../../animations/FadeIn';


//Components
import Style from './styles/style';

class Istaril2 extends React.Component {
    constructor(props){ 
        super(props);
        this.state = { 
            opacity: new Animated.Value(0),
            opacity1: new Animated.Value(0),
            transform: new Animated.Value(0),
            opacity2: new Animated.Value(0),
            transform1: new Animated.Value(0),
            opacity3: new Animated.Value(0),
            transform2: new Animated.Value(0)
        }
        this.Animations = this.Animations.bind(this)
        
    }

    componentDidMount(){
        this.Animations()
    }

    Animations(){
        fadeInLeft({opacity: this.state.opacity, transform: this.state.transform});
        fadeIn(this.state.opacity1);
        fadeInRight({opacity: this.state.opacity2, transform: this.state.transform1});
    }

    render(){
        return(
            <React.Fragment>
                <Image  source={require('../../assets/portada13.jpg')} style={Style.image}/> 
                <ImageFadeIn source={require('../../assets/progre1.png')} styles={ Style.por1 } state={this.state.opacity1}/> 
                <ImageFadeInRight source={require('../../assets/progre3.png')} styles={ Style.por4 } state={{ opacity: this.state.opacity2, transform: this.state.transform1 }}/> 
                <ImageFadeInLeft source={require('../../assets/cuadro.png')} styles={ Style.por2 } state={{ opacity: this.state.opacity, transform: this.state.transform }}/> 
                <Image source={require('../../assets/menu.png')} style={Style.por5} /> 

                <Navigation routes={{ box1: 'Istaril', box2: 'Indice', box3: 'Semanas' }} props={ this.props } />   
            </React.Fragment>
        );   
    }
}

export default Istaril2 ;