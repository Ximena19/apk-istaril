import { StyleSheet } from "react-native";

const Style = StyleSheet.create({
    image: {
        resizeMode: 'stretch',
        width: '100%',
        height: '100%',
    },

    por1: {
        resizeMode: 'stretch',
        width: '60%',
        height: '10%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '4%',
        left: '8%'
    },
    por2: {
        resizeMode: 'stretch',
        width: '80%',
        height: '50%',
        position: 'absolute',
        zIndex: 1,
        top: '37%',
        left: '10%'
    },
    
    por4: {
        resizeMode: 'stretch',
        width: '50%',
        height: '12%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '19%',
        left: '22%'
    },
    por3: {
        resizeMode: 'contain',
        width: '25%',
        height: '20%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '79.9%',
        right: '-1.8%'
    },
    por5: {
        width: '12%',
        height: '15%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '2%',
        right: '10%'
    },
    image1: {
        resizeMode: 'contain',
        width: '14%',
        height: '15%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '40%',
        right: '64.3%'
    },
    image2: {
        resizeMode: 'contain',
        width: '20%',
        height: '17%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '51%',
        right: '47.6%'
    },
    image3: {
        resizeMode: 'contain',
        width: '33%',
        height: '24%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '51%',
        right: '28%'
    },
});

export default Style;
