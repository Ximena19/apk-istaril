import React from 'react';
import { Image, Text, Animated } from 'react-native'; 
import Navigation from '../../shared/navigation';
import { fadeInLeft, ImageFadeInLeft } from '../../animations/FadeInLeft';
import { fadeInRight, ImageFadeInRight } from '../../animations/FadeInRight';
import { fadeIn, ImageFadeIn } from '../../animations/FadeIn';
import { UpView, Up } from '../../animations/Up/index';


//Components
import Style from './styles/style';

class Reduccion extends React.Component {
    constructor(props){ 
        super(props);
        this.state = { 
            /** Fade In Left States */
            opacityFadeInLeft: new Animated.Value(0),
            transformFadeInLeft: new Animated.Value(0),
  
            /** Fade In States */
            opacity: new Animated.Value(0),
            opacity1: new Animated.Value(0),

            /** Fade In Right States */
            opacityFadeInRight: new Animated.Value(0),
            transformFadeInRight: new Animated.Value(0),

            /** Up Lines States (Line 1) */
            line1Opacity: new Animated.Value(0),
            line1Transform: new Animated.Value(-0.1),
    
            /** Up Lines States (Line 2) */
            line2Opacity: new Animated.Value(0),
            line2Transform: new Animated.Value(-0.1),
    
            /** Up Lines States (Line 3) */
            line3Opacity: new Animated.Value(0),
            line3Transform: new Animated.Value(-0.1),
    
            /** Up Lines States (Line 4) */
            line4Opacity: new Animated.Value(0),
            line4Transform: new Animated.Value(-0.1),

            /** Up Lines States (Line 5) */
            line5Opacity: new Animated.Value(0),
            line5Transform: new Animated.Value(-0.1),

            /**fade In  Botton states */
            opacity3: new Animated.Value(0),
            transform2: new Animated.Value(0)
        }
        this.Animations = this.Animations.bind(this)
        
    }

    componentDidMount(){
        this.Animations()
    }

    Animations(){
        fadeInLeft({ opacity: this.state.opacityFadeInLeft, transform: this.state.transformFadeInLeft });
        fadeInRight({ opacity: this.state.opacityFadeInRight, transform: this.state.transformFadeInRight });
        fadeIn(this.state.opacity); 
        fadeIn(this.state.opacity1); 
        // fadeIn(this.state.opacity1);
        // Pulse(this.state.pulse);
        let bandera = 0;
        const {
            line1Opacity,
            line1Transform,
            line2Opacity,
            line2Transform,
            line3Opacity,
            line3Transform,
        } = this.state;
        let i = setInterval(function(){
            if(bandera == 0){
                Up({ opacity: line1Opacity, transform: line1Transform });
                bandera++
            }else if(bandera == 1){
                Up({ opacity: line2Opacity, transform: line2Transform });
                bandera++
            }else if(bandera == 2){
                Up({ opacity: line3Opacity, transform: line3Transform });
                bandera++
            }else if(bandera == 3){ 
                clearInterval(i);
            }
        }, 2000);
    }

    render(){
        return(
            <React.Fragment>
                <Image  source={require('../../assets/portada20.jpg')} style={Style.image}/> 
                <ImageFadeInLeft source={require('../../assets/lio1.png')} styles={ Style.por1 } state={{ opacity: this.state.opacityFadeInLeft, transform: this.state.transformFadeInLeft }}/>  
                <ImageFadeIn source={require('../../assets/numero.png')} styles={ Style.por4 } state={this.state.opacity1}/> 
                <ImageFadeInRight source={require('../../assets/lio3.png')} styles={ Style.por2 } state={{ opacity: this.state.opacityFadeInRight, transform: this.state.transformFadeInRight }}/> 

                 {/*barras  */}
                <UpView  source={require('../../assets/gris1.png')} styles={Style.image1} state={{ opacity: this.state.line1Opacity, transform:this.state.line1Transform }}/> 
                <UpView  source={require('../../assets/naranaj2.png')} styles={Style.image2} state={{ opacity: this.state.line2Opacity, transform:this.state.line2Transform }}/> 
                <UpView  source={require('../../assets/morado2.png')} styles={Style.image3} state={{ opacity: this.state.line3Opacity, transform:this.state.line3Transform }}/> 
                 
                <Image source={require('../../assets/menu.png')} style={Style.por5} />

                <Navigation routes={{ box1: 'Tasa', box2: 'Indice', box3: 'Reduccion2' }} props={ this.props } />   
            </React.Fragment>
        );   
    } 
}

export default Reduccion ;