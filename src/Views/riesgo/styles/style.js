import { StyleSheet } from "react-native";

const Style = StyleSheet.create({
    image: {
        resizeMode: 'stretch',
        width: '100%',
        height: '100%',
    },

    image1: {
        resizeMode: 'stretch',
        width: '64.5%',
        height: '10%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '3%',
        left: '8%'
    },
    image2: {
        resizeMode: 'stretch',
        width: '19%',
        height: '60%',
        backgroundColor: '#00000033',
        position: 'absolute',
        zIndex: 1,
        top: '20%',
        left: '7%'
    },
    image3: {
        resizeMode: 'stretch',
        width: '50%',
        height: '50%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '35%',
        right: '17.5%'
    },
    image4: {
        width: '13%',
        height: '6%',
        resizeMode: 'stretch',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '6%',
        right: '9.4%'
    },
    image5: {
        resizeMode: 'stretch',
        width: '7%',
        height: '40%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 2,
        top: '41%',
        left: '34%'
    },
    image6: {
        resizeMode: 'stretch',
        width: '7%',
        height: '38%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 2,
        top: '43%',
        left: '44%'
    },
    image7: {
        resizeMode: 'stretch',
        width: '7%',
        height: '35%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 2,
        top: '46%',
        left: '54%'
    },
    image8: {
        resizeMode: 'stretch',
        width: '7%',
        height: '23%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 2,
        top: '58%',
        left: '63%'
    },
    image9: {
        resizeMode: 'stretch',
        width: '7%',
        height: '20%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 2,
        top: '61%',
        left: '72%'
    },
    image10: {
        resizeMode: 'stretch',
        width: '10%',
        height: '23%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 2,
        top: '19%',
        left: '33%'
    },
    image11: {
        resizeMode: 'stretch',
        width: '10%',
        height: '23%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 2,
        top: '20%',
        left: '43%'
    },
    image12: {
        resizeMode: 'stretch',
        width: '10%',
        height: '23%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 2,
        top: '21%',
        left: '53%'
    },
    image13: {
        resizeMode: 'stretch',
        width: '10%',
        height: '23%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 2,
        top: '24%',
        left: '62.3%'
    },
    image14: {
        resizeMode: 'stretch',
        width: '10%',
        height: '25%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 2,
        top: '30%',
        left: '72%'
    },
});

export default Style;
