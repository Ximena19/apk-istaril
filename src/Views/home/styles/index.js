import { StyleSheet, Dimensions } from "react-native";

const Style = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        height: '100%',
        textAlign: 'center',
        width: '100%'
    },
    image: {
        width: '100%',
        height: '100%',
        resizeMode: 'stretch',
        backgroundColor: 'black',
        flex: 1,
        padding: 5,
    },
    image1: {
        width: '40%',
        height: '100%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        top: '-6%',
        left: '12.5%'
    },
    image2: {
        width: '27%',
        height: '100%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        zIndex: 1,
        position: 'absolute',
        top: '16%',
        left: '25%'

    },
    image3: {
        width: '50%',
        height: '45%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        zIndex: 1,
        position: 'absolute',
        top: '53%',
        right: '-14.3%'

    },
    
});

export default Style;