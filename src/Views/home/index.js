import React from 'react';
import { Text } from 'react-native';
import { Dimensions } from 'react-native';
import { TouchableOpacity, Image, Animated } from 'react-native';
import { fadeIn, ImageFadeIn } from '../../animations/FadeIn';
import { fadeInLeft, ImageFadeInLeft } from '../../animations/FadeInLeft';
import { fadeInRight, ImageFadeInRight } from '../../animations/FadeInRight';
import Style from './styles';
class Home extends React.Component {
    constructor(props) {
        super(props);

        this.nextPage = this.nextPage.bind(this);

        this.state = { 
            opacity: new Animated.Value(0),
            opacity1: new Animated.Value(0),
            transform: new Animated.Value(0),
            opacity2: new Animated.Value(0),
            transform1: new Animated.Value(0)
        }
    }

    componentDidMount(){
        this.Animations()
    }

    Animations(){
        fadeInLeft({opacity: this.state.opacity, transform: this.state.transform});
        fadeIn(this.state.opacity1);
        fadeInRight({opacity: this.state.opacity2, transform: this.state.transform1});

    }

    nextPage(){
        this.props.navigation.navigate('Indice');
    }

    render(){
        return ( 
            <React.Fragment>
                {/* Background Image */}
                <TouchableOpacity style={Style.container} onPress={() => this.nextPage()}>
                    <Image  source={require('../../assets/portada1.png')} style={Style.image}/> 
                </TouchableOpacity>
                
                {/* Purple Text */}
                <ImageFadeInLeft source={require('../../assets/recurso1.png')} styles={Style.image1} state={{ opacity: this.state.opacity, transform: this.state.transform }} />

                {/* Istaril Logo */}
                <ImageFadeIn source={require('../../assets/recurso2.png')} styles={Style.image2} state={this.state.opacity1}/>

                {/* Right Icons */}
                <ImageFadeInRight source={require('../../assets/recurso3.png')} styles={Style.image3} state={{ opacity: this.state.opacity2, transform: this.state.transform1 }}/>
            </React.Fragment>
        )
    }
}

export default Home;