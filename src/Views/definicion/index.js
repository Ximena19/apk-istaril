import React from 'react';
import { Animated, Image, Text } from 'react-native'; 
import Navigation from '../../shared/navigation';
import { fadeIn, ImageFadeIn } from '../../animations/FadeIn';
import { fadeInLeft, ImageFadeInLeft } from '../../animations/FadeInLeft';



//Components
import Style from './styles/style';
import { fadeInRight, ImageFadeInRight } from '../../animations/FadeInRight';

class Definicion extends React.Component {
    constructor(props){
        super(props);
        this.state = { 
            opacity: new Animated.Value(0),
            opacity1: new Animated.Value(0),
            transform: new Animated.Value(0),
            opacity2: new Animated.Value(0),
            transform1: new Animated.Value(0)
        }

        
    }

    componentDidMount(){
        this.Animations()
    }

    Animations(){
        fadeInRight({opacity: this.state.opacity2, transform: this.state.transform1});
        fadeIn(this.state.opacity1);

    }
    

    render(){
        return(
            <React.Fragment>
                <Image  source={require('../../assets/portada3.jpg')} style={Style.image1}/> 
                <ImageFadeIn  source={require('../../assets/obesidad.png')} styles={Style.image2} state={this.state.opacity1}/> 
                <ImageFadeInRight  source={require('../../assets/grafica.png')} styles={Style.image3} state={{ opacity: this.state.opacity2, transform: this.state.transform1 }}/> 
                <Image source={require('../../assets/menu.png')} style={Style.image4} />
                <Navigation routes={{ box1: 'Indice', box2: 'Indice', box3: 'Sobrepeso'}} props={ this.props } />   
            </React.Fragment>                 
        );   
    }
}

export default Definicion;