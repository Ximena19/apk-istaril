import { Dimensions, StyleSheet } from "react-native";

const Style = StyleSheet.create({
    image1: {
        width: '100%',
        height: '100%',
        resizeMode: 'stretch',
        backgroundColor: 'black',
        flex: 1
    },
    image2: {
        width: '62%',
        height: '70%',
        resizeMode: 'contain',
        flex: 1,
        position: 'absolute',
        top: '-8%',
        left: '19%'
    },
    image3: {
        width: '69%',
        height: '46%',
        resizeMode: 'contain',
        flex: 1,
        position: 'absolute',
        top: '40%',
        left: '15%'
    },
    image4: {
        width: '12%',
        height: '15%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '2%',
        right: '10%'
    },
});

export default Style;