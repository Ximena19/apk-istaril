import React from 'react';
import { Image, Text, Animated } from 'react-native'; 
import Navigation from '../../shared/navigation';
import { fadeInLeft, ImageFadeInLeft } from '../../animations/FadeInLeft';
import { fadeInRight, ImageFadeInRight } from '../../animations/FadeInRight';
import { fadeIn, ImageFadeIn } from '../../animations/FadeIn';
import { fadeInBottom, ImageFadeInBottom } from '../../animations/FadeInBottom';
import { LeftView, Left } from '../../animations/Left/index';


//Components
import Style from './styles/style';

class Istaril extends React.Component {
    constructor(props){ 
        super(props);
        this.state = { 
            opacity: new Animated.Value(0),
            opacity1: new Animated.Value(0),
            transform: new Animated.Value(0),
            opacity2: new Animated.Value(0),
            transform1: new Animated.Value(0),
            opacity3: new Animated.Value(0),
            transform2: new Animated.Value(0),
            lineOpacity: new Animated.Value(0),
            lineTransform: new Animated.Value(0)
        }
        this.Animations = this.Animations.bind(this)
        
    }

    componentDidMount(){
        this.Animations()
    }

    Animations(){
        const { lineOpacity, lineTransform } = this.state;
        fadeInLeft({opacity: this.state.opacity, transform: this.state.transform});
        fadeIn(this.state.opacity1);
        fadeInRight({opacity: this.state.opacity2, transform: this.state.transform1});
        fadeInBottom({opacity: this.state.opacity3, transform: this.state.transform2});
        Left({ opacity: lineOpacity, transform: lineTransform });
    }

    render(){
        return(
            <React.Fragment>
                <Image  source={require('../../assets/fondo12.jpg')} style={Style.image}/> 
                <ImageFadeInLeft source={require('../../assets/progre1.png')} styles={ Style.por1 } state={{ opacity: this.state.opacity, transform: this.state.transform }}/> 
                <ImageFadeInRight source={require('../../assets/progre3.png')} styles={ Style.por4 }  state={{ opacity: this.state.opacity2, transform: this.state.transform1 }}/> 
                <LeftView source={require('../../assets/grafica0.png')} styles={ Style.por2 } state={{ opacity: this.state.lineOpacity, transform: this.state.lineTransform }}/> 
                <Image  source={require('../../assets/fondo12.jpg')} style={Style.image}/> 

                <ImageFadeInRight source={require('../../assets/cua1.png')} styles={ Style.image1 }  state={{ opacity: this.state.opacity2, transform: this.state.transform1 }}/> 

                <Image source={require('../../assets/menu.png')} style={Style.por5} />
                <Navigation routes={{ box1: 'Perfil2', box2: 'Indice', box3: 'Istaril2' }} props={ this.props } />   
            </React.Fragment>
        );   
    }
}

export default Istaril ;