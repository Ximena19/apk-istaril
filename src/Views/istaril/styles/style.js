import { StyleSheet } from "react-native";

const Style = StyleSheet.create({
    image: {
        resizeMode: 'stretch',
        width: '100%',
        height: '100%',
        backgroundColor: 'black'
    },

    por1: {
        resizeMode: 'stretch',
        width: '65%',
        height: '10%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '3%',
        left: '9%'
    },
    por2: {
        resizeMode: 'stretch',
        width: '65%',
        height: '50%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '30%',
        left: '5%'
    },
    por3: {
        resizeMode: 'stretch',
        width: '10%',
        height: '17%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        bottom: '13%',
        right: '4%'
    },
    por4: {
        resizeMode: 'stretch',
        width: '33%',
        height: '7%',
        position: 'absolute',
        zIndex: 1,
        top: '19%',
        left: '30%'
    },
    por5: {
        width: '12%',
        height: '15%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '2%',
        right: '10%'
    },

    image1: {
        resizeMode: 'stretch',
        width: '25%',
        height: '48%',
        position: 'absolute',
        zIndex: 1,
        top: '31%',
        left: '70.3%'
    },
});

export default Style;
