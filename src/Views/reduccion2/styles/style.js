import { StyleSheet } from "react-native";

const Style = StyleSheet.create({
    image: {
        resizeMode: 'stretch',
        width: '100%',
        height: '100%'
    },

    por1: {
        resizeMode: 'stretch',
        width: '60%',
        height: '8%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '5%',
        left: '8%'
    },
    por2: {
        resizeMode: 'stretch',
        width: '25%',
        height: '48%',
        position: 'absolute',
        zIndex: 1,
        top: '25%',
        left: '7.5%'
    },
    
    por4: {
        resizeMode: 'stretch',
        width: '82%',
        height: '57%',
        position: 'absolute',
        zIndex: 1,
        top: '25%',
        right: '10%'
    },
    por3: {
        resizeMode: 'stretch',
        width: '25%',
        height: '48%',
        position: 'absolute',
        zIndex: 1,
        top: '25%',
        left: '40%'
    },
    por5: {
        width: '12%',
        height: '15%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '2%',
        right: '10%'
    },
});

export default Style;
