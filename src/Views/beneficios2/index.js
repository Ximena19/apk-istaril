import React from 'react';
import { Image, Text, Animated } from 'react-native'; 
import Navigation from '../../shared/navigation';
import { fadeInLeft, ImageFadeInLeft } from '../../animations/FadeInLeft';
import { fadeIn, ImageFadeIn } from '../../animations/FadeIn';
import { fadeInBottom, ImageFadeInBottom } from '../../animations/FadeInBottom';
import { UpView, Up } from '../../animations/Up/index';



//Components
import Style from './styles/style';

class Beneficios2 extends React.Component {
    constructor(props){ 
        super(props);
        this.state = { 
            /** Fade In Left States */
            opacityFadeInLeft: new Animated.Value(0),
            transformFadeInLeft: new Animated.Value(0),

            /** Fade In States */
            opacity: new Animated.Value(0),
            opacity1: new Animated.Value(0),

            /** Fade In Right States */
            opacityFadeInRight: new Animated.Value(0),
            transformFadeInRight: new Animated.Value(0),

            /** Up Lines States (Line 1) */
            line1Opacity: new Animated.Value(0),
            line1Transform: new Animated.Value(-0.1),
    
            /** Up Lines States (Line 2) */
            line2Opacity: new Animated.Value(0),
            line2Transform: new Animated.Value(-0.1),
    
            /** Up Lines States (Line 3) */
            line3Opacity: new Animated.Value(0),
            line3Transform: new Animated.Value(-0.1),
    
            /** Up Lines States (Line 4) */
            line4Opacity: new Animated.Value(0),
            line4Transform: new Animated.Value(-0.1),

            /** Up Lines States (Line 5) */
            line5Opacity: new Animated.Value(0),
            line5Transform: new Animated.Value(-0.1),

            /**fade In  Botton states */
            opacity3: new Animated.Value(0),
            transform2: new Animated.Value(0)
        }
        this.Animations = this.Animations.bind(this)
        
    }

    componentDidMount(){
        this.Animations()
    }

    Animations(){
        fadeInBottom({opacity: this.state.opacity3, transform: this.state.transform2});
        fadeIn(this.state.opacity); 
        fadeIn(this.state.opacity1); 
        // fadeIn(this.state.opacity1);
        // Pulse(this.state.pulse);
        let bandera = 0;
        const {
            line1Opacity,
            line1Transform,
            line2Opacity,
            line2Transform,
        } = this.state;
        let i = setInterval(function(){
            if(bandera == 0){
                Up({ opacity: line1Opacity, transform: line1Transform });
                bandera++
            }else if(bandera == 1){
                Up({ opacity: line2Opacity, transform: line2Transform });
                bandera++
            }else if(bandera == 2){
                clearInterval(i);
            }
        }, 2000);
    }

    render(){
        return(
            <React.Fragment>
                <Image  source={require('../../assets/portada17.jpg')} style={Style.image}/> 
                <ImageFadeIn source={require('../../assets/años.png')} styles={ Style.por1 } state={this.state.opacity1}/>  
                <ImageFadeInBottom source={require('../../assets/años1.png')} styles={ Style.por4 } state={{ opacity: this.state.opacity3, transform: this.state.transform2 }}/> 
                <UpView source={require('../../assets/ur1.png')} styles={ Style.por2 } state={{ opacity: this.state.line1Opacity, transform:this.state.line1Transform }}/>  
 
                <UpView  source={require('../../assets/uri2.png')} styles={Style.image6} state={{ opacity: this.state.line2Opacity, transform:this.state.line2Transform }}/> 

                <Image source={require('../../assets/menu.png')} style={Style.por3} />
                <Image  source={require('../../assets/amarillo.png')} style={Style.image2}/> 

                <Navigation routes={{ box1: 'Beneficios', box2: 'Indice', box3: 'Beneficios3' }} props={ this.props } />   
            </React.Fragment>
        );   
    }
}

export default Beneficios2 ;