import { StyleSheet } from "react-native";

const Style = StyleSheet.create({
    image: {
        resizeMode: 'stretch',
        width: '100%',
        height: '100%'
    },

    por1: {
        resizeMode: 'stretch',
        width: '70%',
        height: '10%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '3%',
        left: '10%'
    },
    por2: {
        resizeMode: 'stretch',
        width: '70%',
        height: '55%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        bottom: '17%',
        left: '6%'
    },
    
    por4: {
        resizeMode: 'stretch',
        width: '30%',
        height: '7%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '18%',
        left: '36%'
    },
    por3: {
        width: '12%',
        height: '15%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '2%',
        right: '10%'
    },
    image2: {
        resizeMode: 'stretch',
        width: '14%',
        height: '24%',
        position: 'absolute',
        zIndex: 1,
        bottom: '0%',
        right: '0%'
    },
    image6: {
        resizeMode: 'stretch',
        width: '38%',
        height: '53%',
        position: 'absolute',
        zIndex: 1,
        top: '31%',
        left: '59.9%'
    },
});

export default Style;
