import React from 'react';
import { Image, Text, Animated } from 'react-native'; 
import Navigation from '../../shared/navigation';
import { fadeInLeft, ImageFadeInLeft } from '../../animations/FadeInLeft';
import { fadeIn, ImageFadeIn } from '../../animations/FadeIn';


//Components
import Style from './styles/style';

class Tasa extends React.Component {
    constructor(props){ 
        super(props);
        this.state = { 
            opacity: new Animated.Value(0),
            opacity1: new Animated.Value(0),
            transform: new Animated.Value(0),
            opacity2: new Animated.Value(0),
            transform1: new Animated.Value(0),
            opacity3: new Animated.Value(0),
            transform2: new Animated.Value(0)
        }
        this.Animations = this.Animations.bind(this)    
        
    }
  
    componentDidMount(){
        this.Animations()
    }

    Animations(){
        fadeInLeft({opacity: this.state.opacity, transform: this.state.transform});
        fadeIn(this.state.opacity1);

    }

    render(){
        return(
            <React.Fragment>
                <Image  source={require('../../assets/PORTADA19.jpg')} style={Style.image}/> 
                <ImageFadeInLeft source={require('../../assets/REDU1.png')} styles={ Style.por1 } state={{ opacity: this.state.opacity, transform: this.state.transform }}/>  
                {/* <Image source={require('../../assets/años1.png')} style={ Style.por4 }/>  */}
                <ImageFadeIn source={require('../../assets/REDU2.png')} styles={ Style.por2 } state={this.state.opacity1}/>  
                <Image source={require('../../assets/menu.png')} style={Style.por3} />

                <Navigation routes={{ box1: 'Beneficios3', box2: 'Indice', box3: 'Reduccion' }} props={ this.props } />   
            </React.Fragment>
        );   
    }
}

export default Tasa ;