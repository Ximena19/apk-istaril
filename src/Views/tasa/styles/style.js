import { StyleSheet } from "react-native";

const Style = StyleSheet.create({
    image: {
        resizeMode: 'stretch',
        width: '100%',
        height: '100%'
    },

    por1: {
        resizeMode: 'stretch',
        width: '60%',
        height: '10%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '3%',
        left: '8%'
    },
    por2: {
        resizeMode: 'stretch',
        width: '86%',
        height: '60%',
        position: 'absolute',
        zIndex: 1,
        top: '20%',
        left: '6%'
    },
    
    por4: {
        resizeMode: 'contain',
        width: '20%',
        height: '20%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '9%',
        left: '36%'
    },
    por3: {
        width: '12%',
        height: '15%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '2%',
        right: '10%'
    },
});

export default Style;
