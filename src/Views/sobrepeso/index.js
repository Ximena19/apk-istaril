import React from 'react';
import { Animated, Image, Text } from 'react-native'; 
import Navigation from '../../shared/navigation';
import { fadeInLeft, ImageFadeInLeft } from '../../animations/FadeInLeft';
import { fadeInRight, ImageFadeInRight } from '../../animations/FadeInRight';
import { fadeIn, ImageFadeIn } from '../../animations/FadeIn';
import { fadeInBottom, ImageFadeInBottom } from '../../animations/FadeInBottom';
import { Rotate, ImageRotate } from '../../animations/Rotate';


//Components
import Style from './styles/style';

class Sobrepeso extends React.Component {
    constructor(props){
        super(props);
        this.state = { 
            opacity: new Animated.Value(0),
            opacity1: new Animated.Value(0),
            transform: new Animated.Value(0),
            opacity2: new Animated.Value(0),
            transform1: new Animated.Value(0),
            opacity3: new Animated.Value(0),
            transform2: new Animated.Value(0),
            rotate: new Animated.Value(0),
        }
        this.Animations = this.Animations.bind(this)
        
    }

    componentDidMount(){
        this.Animations()
    }

    Animations(){
        fadeInLeft({opacity: this.state.opacity, transform: this.state.transform});
        fadeIn(this.state.opacity1);
        fadeInRight({opacity: this.state.opacity2, transform: this.state.transform1});
        fadeInBottom({opacity: this.state.opacity3, transform: this.state.transform2});
        Rotate(this.state.rotate);

    }

    render(){
        return(
            <React.Fragment>
                <Image  source={require('../../assets/portada4.jpg')} style={Style.image2} /> 
                <ImageFadeInLeft source={require('../../assets/text.png')} styles={ Style.image3 } state={{ opacity: this.state.opacity, transform: this.state.transform }}/>
                <ImageFadeIn source={require('../../assets/cuerpo.png')} styles={ Style.image4 } state={this.state.opacity1}/>

                <ImageRotate source={require('../../assets/derecha.png')} styles={ Style.image5 } state={this.state.rotate}/>
                <ImageRotate source={require('../../assets/izquierda.png')} styles={ Style.image6 } state={this.state.rotate}/>

                <ImageFadeInRight source={require('../../assets/text5.png')} styles={ Style.image7 } state={{ opacity: this.state.opacity2, transform: this.state.transform1 }}/>
                <ImageFadeInBottom source={require('../../assets/text6.png')} styles={ Style.image8 } state={{ opacity: this.state.opacity3, transform: this.state.transform2 }}/>
                <Image source={require('../../assets/menu.png')} style={Style.image9} />

                <Navigation routes={{ box1: 'Definicion', box2: 'Indice', box3: 'Riesgo' }} props={ this.props } />   
            </React.Fragment>
        );   
    }
}

export default Sobrepeso;