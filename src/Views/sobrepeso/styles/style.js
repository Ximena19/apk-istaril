import { StyleSheet } from "react-native";

const Style = StyleSheet.create({
    image2: {
        resizeMode: 'stretch',
        width: '100%',
        height: '100%',
    },
    image3: {
        resizeMode: 'stretch',
        width: '69%',
        height: '12%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '2%',
        left: '10%'
    },
    image4: {
        resizeMode: 'stretch',
        width: '50%',
        height: '80.1%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '15.6%',
        left: '28%'
    },
    image5: {
        resizeMode: 'contain',
        width: '50%',
        height: '55%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '24.6%',
        left: '46.8%'
    },
    image6: {
        resizeMode: 'contain',
        width: '50%',
        height: '55%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '32%',
        left: '5%'
    },
    image7: {
        resizeMode: 'stretch',
        width: '20%',
        height: '43%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '31%',
        right: '6%'
    },
    image8: {
        resizeMode: 'stretch',
        width: '23%',
        height: '54%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '32%',
        left: '5%'
    },
    image9: {
        width: '12%',
        height: '15%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '2%',
        right: '10%'
    },
});

export default Style;