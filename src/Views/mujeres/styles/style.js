import { Dimensions, StyleSheet } from "react-native";

const Style = StyleSheet.create({
    image: {
        resizeMode: 'stretch',
        width: '100%',
        height: '100%'
    },

    pol3: {
        resizeMode: 'stretch',
        width: '71%',
        height: '10%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '3%',
        left: '8%'
    },
    pol1: {
        resizeMode: 'stretch',
        width: '15%',
        height: '20%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '19%',
        left: '0%'
    },
    pol2: {
        resizeMode: 'stretch',
        width: '65%',
        height: '60%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '25%',
        left: '15%'
    },
    pol4: {
        resizeMode: 'stretch',
        width: '9%',
        height: '18%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        bottom: '25%',
        right: '3%'
    },
    pol5: {
        width: '12%',
        height: '15%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '2%',
        right: '10%'
    },
    image1: {
        width: '25%',
        height: '35%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '24%',
        right: '61.3%'
    },
    image2: {
        width: '26%',
        height: '35%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '24%',
        right: '49.6%'
    },
    image3: {
        width: '26%',
        height: '31%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '24%',
        right: '39%'
    },
    image4: {
        width: '26%',
        height: '31%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '24%',
        right: '28%'
    },
    image5: {
        width: '25%',
        height: '30%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '24%',
        right: '14.7%'
    },
    image6: {
        width: '15%',
        height: '13%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '57%',
        right: '65.5%'
    },
    image7: {
        width: '15%',
        height: '15%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '61%',
        right: '54%'
    },
    image8: {
        width: '14%',
        height: '13%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '54%',
        right: '44.3%'
    },
    image9: {
        width: '17%',
        height: '14%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '57%',
        right: '31.6%'
    },
    image10: {
        width: '17%',
        height: '15%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '54%',
        right: '17.5%'
    },
});

export default Style;
