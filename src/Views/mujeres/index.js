import React from 'react';
import { Image, Text, Animated } from 'react-native'; 
import Navigation from '../../shared/navigation';
import { fadeInLeft, ImageFadeInLeft } from '../../animations/FadeInLeft';
import { fadeInRight, ImageFadeInRight } from '../../animations/FadeInRight';
import { fadeIn, ImageFadeIn } from '../../animations/FadeIn';
import { fadeInBottom, ImageFadeInBottom } from '../../animations/FadeInBottom';
import { UpView, Up } from '../../animations/Up/index';




//Components
import Style from './styles/style';

class Mujeres extends React.Component {
    constructor(props){ 
        super(props);
        this.state = { 
            /** Fade In Left States */
            opacityFadeInLeft: new Animated.Value(0),
            transformFadeInLeft: new Animated.Value(0),

            /** Fade In States */
            opacity: new Animated.Value(0),
            opacity1: new Animated.Value(0),

            /** Fade In Right States */
            opacityFadeInRight: new Animated.Value(0),
            transformFadeInRight: new Animated.Value(0),

            /** Up Lines States (Line 1) */
            line1Opacity: new Animated.Value(0),
            line1Transform: new Animated.Value(-0.1),
    
            /** Up Lines States (Line 2) */
            line2Opacity: new Animated.Value(0),
            line2Transform: new Animated.Value(-0.1),
    
            /** Up Lines States (Line 3) */
            line3Opacity: new Animated.Value(0),
            line3Transform: new Animated.Value(-0.1),
    
            /** Up Lines States (Line 4) */
            line4Opacity: new Animated.Value(0),
            line4Transform: new Animated.Value(-0.1),

            /** Up Lines States (Line 5) */
            line5Opacity: new Animated.Value(0),
            line5Transform: new Animated.Value(-0.1),

            /**fade In  Botton states */
            opacity3: new Animated.Value(0),
            transform2: new Animated.Value(0)

        }
        this.Animations = this.Animations.bind(this)
        
    }

    componentDidMount(){
        this.Animations()
    }

    Animations(){
        fadeInLeft({ opacity: this.state.opacityFadeInLeft, transform: this.state.transformFadeInLeft });
        fadeIn(this.state.opacity); 
        fadeIn(this.state.opacity1); 
        fadeInRight({ opacity: this.state.opacityFadeInRight, transform: this.state.transformFadeInRight });
        fadeInBottom({opacity: this.state.opacity3, transform: this.state.transform2});

        // fadeIn(this.state.opacity1);
        // Pulse(this.state.pulse);
        let bandera = 0;
        const {
            line1Opacity,
            line1Transform,
            line2Opacity,
            line2Transform,
            line3Opacity,
            line3Transform,
            line4Opacity,
            line4Transform,
            line5Opacity,
            line5Transform,
        } = this.state;
        let i = setInterval(function(){
            if(bandera == 0){
                Up({ opacity: line1Opacity, transform: line1Transform });
                bandera++
            }else if(bandera == 1){
                Up({ opacity: line2Opacity, transform: line2Transform });
                bandera++
            }else if(bandera == 2){
                Up({ opacity: line3Opacity, transform: line3Transform });
                bandera++
            }else if(bandera == 3){
                Up({ opacity: line4Opacity, transform: line4Transform });
                bandera++
            }else if(bandera == 4){ 
                Up({ opacity: line5Opacity, transform: line5Transform });
                bandera++
            }else if(bandera == 5){ 
                clearInterval(i);
            }
        }, 2000);
    }

    render(){
        return(
            <React.Fragment>
                <Image  source={require('../../assets/portada7.jpg')} style={Style.image}/> 
                <ImageFadeIn source={require('../../assets/pol3.png')} styles={ Style.pol3 } state={this.state.opacity}/>
                 <ImageFadeInLeft source={require('../../assets/pol1.png')} styles={ Style.pol1 } state={{ opacity: this.state.opacityFadeInLeft, transform: this.state.transformFadeInLeft }}/>   
                 <Image source={require('../../assets/pol2.png')} style={ Style.pol2 }/> 
                 {/* barras */}
                <UpView  source={require('../../assets/Rec1.png')} styles={Style.image1} state={{ opacity: this.state.line1Opacity, transform:this.state.line1Transform }}/> 
                <UpView  source={require('../../assets/Rec2.png')} styles={Style.image2} state={{ opacity: this.state.line2Opacity, transform:this.state.line2Transform }}/> 
                <UpView  source={require('../../assets/Rec4.png')} styles={Style.image3} state={{ opacity: this.state.line3Opacity, transform:this.state.line3Transform }}/> 
                <UpView  source={require('../../assets/Rec5.png')} styles={Style.image4} state={{ opacity: this.state.line4Opacity, transform:this.state.line4Transform }}/> 
                <UpView  source={require('../../assets/Rec6.png')} styles={Style.image5} state={{ opacity: this.state.line5Opacity, transform:this.state.line5Transform }}/> 

                {/* botones */}
                <ImageFadeIn  source={require('../../assets/Rec0.png')} styles={Style.image6} state={this.state.opacity1}/> 
                <ImageFadeIn  source={require('../../assets/Rec7.png')} styles={Style.image7} state={this.state.opacity1}/> 
                <ImageFadeIn  source={require('../../assets/Rec8.png')} styles={Style.image8} state={this.state.opacity1}/> 
                <ImageFadeIn  source={require('../../assets/Rec9.png')} styles={Style.image9} state={this.state.opacity1}/> 
                <ImageFadeIn  source={require('../../assets/Rec10.png')} styles={Style.image10} state={this.state.opacity1}/> 

                 <ImageFadeInBottom source={require('../../assets/pol4.png')} styles={ Style.pol4 } state={{ opacity: this.state.opacity3, transform: this.state.transform2 }}/>   
                 <Image source={require('../../assets/menu.png')} style={Style.pol5} />

                <Navigation routes={{ box1: 'Evolucion', box2: 'Indice', box3: 'Cerebro' }} props={ this.props } />   
            </React.Fragment>
        );   
    }
}

export default Mujeres;