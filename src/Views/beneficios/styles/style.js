import { StyleSheet } from "react-native";

const Style = StyleSheet.create({
    image: {
        resizeMode: 'stretch',
        width: '100%',
        height: '100%'
    },

    por1: {
        resizeMode: 'stretch',
        width: '65%',
        height: '10%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '3%',
        left: '10%'
    },
    por2: {
        resizeMode: 'stretch',
        width: '80%',
        height: '50%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '29%',
        left: '10%'
    },
    
    por4: {
        resizeMode: 'stretch',
        width: '28%',
        height: '10%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '18%',
        left: '36%'
    },
    por5: {
        width: '12%',
        height: '15%',
        resizeMode: 'contain',
        backgroundColor: 'transparent',
        flex: 1,
        position: 'absolute',
        zIndex: 1,
        bottom: '2%',
        right: '10%'
    },

    image1: {
        resizeMode: 'stretch',
        width: '19%',
        height: '40%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '30%',
        left: '24%'
    },
    image2: {
        resizeMode: 'stretch',
        width: '19.8%',
        height: '25%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '45%',
        left: '43%'
    },
    image3: {
        resizeMode: 'stretch',
        width: '40%',
        height: '40%',
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        top: '30%',
        left: '43%'
    },
});

export default Style;
