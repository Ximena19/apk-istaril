import React from 'react'
import { Dimensions } from 'react-native'
import { StyleSheet, View } from 'react-native'

export default function Navigation({ routes, props }) {
    return (
        <React.Fragment>
            <View style={Style.cuadro13} onStartShouldSetResponder={() => props.navigation.navigate(routes.box1)}></View>    
            <View style={Style.cuadro14} onStartShouldSetResponder={() => props.navigation.navigate(routes.box2)}></View>    
            <View style={Style.cuadro15} onStartShouldSetResponder={() => props.navigation.navigate(routes.box3)}></View>  
        </React.Fragment>
    )
}

const Style = StyleSheet.create({
    
    /**navigation */
    cuadro13: {
        width: '3%',
        height: '6%',
        position: 'absolute',
        zIndex: 10,
        top: '87.3%',
        right: '19.2%',
    },
    cuadro14: {
        width: '3%',
        height: '6%',
        position: 'absolute',
        zIndex: 10,
        top: '87.3%',
        right: '14.5%',
    },
    cuadro15: {
        width: '3%',
        height: '6%',
        position: 'absolute',
        zIndex: 10,
        top: '87.3%',
        right: '10%',
    },
})