import React from 'react';
import { Animated } from "react-native";

export function fadeIn(opacity, loop){
    if(loop) {
        Animated.loop(Animated.timing(opacity, {
            toValue: 1,
            duration: 4000,
            useNativeDriver: true
        })).start();
    }else{
        Animated.timing(opacity, {
            toValue: 1,
            duration: 4000,
            useNativeDriver: true
        }).start();
    }
    
}

export function ImageFadeIn({source, styles, state}){
    return(
        <Animated.Image
            source={source}
            style={[styles, { opacity: state }]}
        />
    );
}

export function TextFadeIn({text, styles, state}){
    return(
        <Animated.Text
            style={[styles, { opacity: state }]}
        >{text}</Animated.Text>
    );
}