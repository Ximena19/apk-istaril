import React from 'react';
import { Animated } from 'react-native';

export function Pulse(state){
    Animated.loop(Animated.timing(state, {
        toValue: 1,
        duration: 2000,
        useNativeDriver: true,
    })).start();
}

export function ImagePulse({ source, styles, state }){
    return(
        <Animated.Image
            source={source}
            style={[styles, { transform: [{ scaleX: state.interpolate({
                inputRange: [0, 0.5, 1],
                outputRange: [1.05, 1, 1.05]
            }) }] }]}
        />
    );
}