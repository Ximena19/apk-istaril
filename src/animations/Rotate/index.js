import React from 'react';
import { Animated } from 'react-native';

export function Rotate(state){
    Animated.loop(Animated.timing(state, {
        toValue: 1,
        duration: 2000,
        useNativeDriver: true,
    })).start();
}

export function ImageRotate({ source, styles, state }){
    return(
        <Animated.Image
            source={source}
            style={[styles, { transform: [{ rotate: state.interpolate({
                inputRange: [0, 0.5, 1],
                outputRange: ['0deg', '10deg', '0deg']
            }) }] }]}
        />
    );
}