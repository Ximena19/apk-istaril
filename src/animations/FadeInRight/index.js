import React from 'react';
import { Animated } from "react-native";

export function fadeInRight(state){
    const {
        opacity, transform
    } = state
    Animated.timing(opacity, {
        toValue: 1,
        duration: 2000,
        useNativeDriver: true
    }).start();

    Animated.timing(transform, {
        toValue: 1,
        duration: 2000,
        useNativeDriver: true
    }).start()
}

export function ImageFadeInRight({source, styles, state}){
    return (
        <Animated.Image 
            source={source}
            style={[styles, { opacity: state.opacity, transform: [{ translateX: state.transform.interpolate({
                inputRange: [0,1],
                outputRange: [500, 0]
            }) }] }]}
        />
    );
    
}