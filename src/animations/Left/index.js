import React from 'react';
import { Animated } from "react-native";

export function Left(state){
    const {
        opacity, transform
    } = state
    Animated.timing(opacity, {
        toValue: 1,
        duration: 4000,
        useNativeDriver: true
    }).start();

    Animated.timing(transform, {
        toValue: 1,
        duration: 3000,
        useNativeDriver: true
    }).start()
}

export function LeftView({source, styles, state }){
    return (
        <Animated.Image 
            source={source}
            style={[styles, { opacity: state.opacity, transform: [{ scaleX: state.transform }] }]}
        />
    );
    
}