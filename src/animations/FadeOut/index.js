import React from 'react';
import { Animated } from "react-native";

export function fadeOut(opacity){
    Animated.timing(opacity, {
        toValue: 0,
        duration: 1000,
        useNativeDriver: true
    }).start();
}

export function ImageFadeOut({source, styles, state}){
    return(
        <Animated.Image
            source={source}
            style={[styles, { opacity: state }]}
        />
    );
}

export function TextFadeOut({text, styles, state}){
    return(
        <Animated.Text
            style={[styles, { opacity: state }]}
        >{text}</Animated.Text>
    );
}

export function ViewFadeOut({styles, state}){
    return(
        <Animated.View
            style={[styles, { opacity: state }]}
        />
    );
}