import React from 'react';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from '../Views/home';
import Indice from '../Views/indice';
import Definicion from '../Views/definicion';
import Sobrepeso from '../Views/sobrepeso';
import Riesgo from '../Views/riesgo';
import Evolucion from '../Views/evolucion';
import Mujeres from '../Views/mujeres';
import Cerebro from '../Views/cerebro';
import Cerebro2 from '../Views/cerebro2';
import Perfil from '../Views/perfil';
import Perfil2 from '../Views/perfil2';
import Istaril from '../Views/istaril';
import Istaril2 from '../Views/istaril2';
import Semanas from '../Views/semanas';
import Semanas2 from '../Views/semanas2';
import Beneficios from '../Views/beneficios';
import Beneficios2 from '../Views/beneficios2';
import Beneficios3 from '../Views/beneficios3';
import Tasa from '../Views/tasa';
import Reduccion from '../Views/reduccion';
import Reduccion2 from '../Views/reduccion2';
import Reduccion3 from '../Views/reduccion3';


const Stack = createStackNavigator();

const MyTheme = {
    ...DefaultTheme,
    colors: {
        ...DefaultTheme.colors,
        background: 'white',
        primary: '#fff'
    },
};



export default function Routes() {
    return (
        <NavigationContainer theme={MyTheme}>
            <Stack.Navigator headerMode={false} screenOptions={{
                cardStyle: { backgroundColor: 'transparent', shadowColor: 'transparent' },
                transparentCard: true,
            }}>
                <Stack.Screen name="Home" component={Home} />
                <Stack.Screen name="Indice" component={Indice} />
                <Stack.Screen name="Definicion" component={Definicion} />
                <Stack.Screen name="Sobrepeso" component={Sobrepeso} />
                <Stack.Screen name="Riesgo" component={Riesgo} />
                <Stack.Screen name="Evolucion" component={Evolucion} />
                <Stack.Screen name="Mujeres" component={Mujeres} />
                <Stack.Screen name="Cerebro" component={Cerebro} />
                <Stack.Screen name="Cerebro2" component={Cerebro2} />
                <Stack.Screen name="Perfil" component={Perfil} />
                <Stack.Screen name="Perfil2" component={Perfil2} />
                <Stack.Screen name="Istaril" component={Istaril} />
                <Stack.Screen name="Istaril2" component={Istaril2} />
                <Stack.Screen name="Semanas" component={Semanas} />
                <Stack.Screen name="Semanas2" component={Semanas2} />
                <Stack.Screen name="Beneficios" component={Beneficios} />
                <Stack.Screen name="Beneficios2" component={Beneficios2} />
                <Stack.Screen name="Beneficios3" component={Beneficios3} />
                <Stack.Screen name="Reduccion" component={Reduccion} />
                <Stack.Screen name="Reduccion2" component={Reduccion2} />
                <Stack.Screen name="Reduccion3" component={Reduccion3} />
                <Stack.Screen name="Tasa" component={Tasa} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}



